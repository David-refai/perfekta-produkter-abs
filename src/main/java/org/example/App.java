package org.example;

import org.example.service.PersonsSrv;
import org.example.utility.Utils;

import static java.lang.System.*;


public class App {
    private static App appInstance;
    private static final PersonsSrv personsSrv = PersonsSrv.getInstance();
    private App() {
    }

    public static App getInstance() {
        if (appInstance == null) {
            appInstance = new App();
        }
        return appInstance;
    }


    public static void system() {
//       Print Welcome to the Person Management System
        printBanner();
        var loop = true;
        while (loop) {
            Utils.menu("1️⃣-Add Person", "2️⃣-Update Person", "3️⃣-Delete Person", "4️⃣-Show All Persons", "5️⃣-Search", "6️⃣-calculateAverageSalaryByGender(1️⃣→Male 🧔 OR 2️⃣→Female 👩🏻‍🦰)", "7️⃣-Exit");
            try {
                var input = Utils.printMenu("Choose an option: ");
                switch (Integer.parseInt(input)) {
                    case 1 -> personsSrv.savePerson();
                    case 2 -> personsSrv.update();
                    case 3 -> personsSrv.delete();
                    case 4 -> personsSrv.findAll();
                    case 5 -> personsSrv.search();
                    case 6 -> out.println(personsSrv.calculateAverageSalaryByGender(personsSrv.getPersonDoaImpl().findAll()));
                    case 7 -> {
                        out.println("Goodbye 👋🏻");
                        loop = false;
                    }
                    default -> out.println("Invalid input");
                }
            } catch (NumberFormatException e) {
                out.println("Invalid input");

            }
        }
    }


    private static void printBanner() {
        String banner = """
                                                    ██╗    ██╗███████╗██╗      ██████╗ ██████╗ ███╗   ███╗███████╗    ████████╗ ██████╗                                                                                                                    \s
                                                    ██║    ██║██╔════╝██║     ██╔════╝██╔═══██╗████╗ ████║██╔════╝    ╚══██╔══╝██╔═══██╗                                                                                                                   \s
                                                    ██║ █╗ ██║█████╗  ██║     ██║     ██║   ██║██╔████╔██║█████╗         ██║   ██║   ██║                                                                                                                   \s
                                                    ██║███╗██║██╔══╝  ██║     ██║     ██║   ██║██║╚██╔╝██║██╔══╝         ██║   ██║   ██║                                                                                                                   \s
                                                    ╚███╔███╔╝███████╗███████╗╚██████╗╚██████╔╝██║ ╚═╝ ██║███████╗       ██║   ╚██████╔╝                                                                                                                   \s
                                                     ╚══╝╚══╝ ╚══════╝╚══════╝ ╚═════╝ ╚═════╝ ╚═╝     ╚═╝╚══════╝       ╚═╝    ╚═════╝                                                                                                                    \s
                                                                                                                                                                                                                                                           \s
                ████████╗██╗  ██╗███████╗    ██████╗ ███████╗██████╗ ███████╗ ██████╗ ███╗   ██╗    ███╗   ███╗ █████╗ ███╗   ██╗███████╗ ██████╗ ███████╗███╗   ███╗███████╗███╗   ██╗████████╗    ███████╗██╗   ██╗███████╗████████╗███████╗███╗   ███╗  \s
                ╚══██╔══╝██║  ██║██╔════╝    ██╔══██╗██╔════╝██╔══██╗██╔════╝██╔═══██╗████╗  ██║    ████╗ ████║██╔══██╗████╗  ██║██╔════╝██╔════╝ ██╔════╝████╗ ████║██╔════╝████╗  ██║╚══██╔══╝    ██╔════╝╚██╗ ██╔╝██╔════╝╚══██╔══╝██╔════╝████╗ ████║  \s
                   ██║   ███████║█████╗      ██████╔╝█████╗  ██████╔╝███████╗██║   ██║██╔██╗ ██║    ██╔████╔██║███████║██╔██╗ ██║█████╗  ██║  ███╗█████╗  ██╔████╔██║█████╗  ██╔██╗ ██║   ██║       ███████╗ ╚████╔╝ ███████╗   ██║   █████╗  ██╔████╔██║  \s
                   ██║   ██╔══██║██╔══╝      ██╔═══╝ ██╔══╝  ██╔══██╗╚════██║██║   ██║██║╚██╗██║    ██║╚██╔╝██║██╔══██║██║╚██╗██║██╔══╝  ██║   ██║██╔══╝  ██║╚██╔╝██║██╔══╝  ██║╚██╗██║   ██║       ╚════██║  ╚██╔╝  ╚════██║   ██║   ██╔══╝  ██║╚██╔╝██║  \s
                   ██║   ██║  ██║███████╗    ██║     ███████╗██║  ██║███████║╚██████╔╝██║ ╚████║    ██║ ╚═╝ ██║██║  ██║██║ ╚████║███████╗╚██████╔╝███████╗██║ ╚═╝ ██║███████╗██║ ╚████║   ██║       ███████║   ██║   ███████║   ██║   ███████╗██║ ╚═╝ ██║██╗
                   ╚═╝   ╚═╝  ╚═╝╚══════╝    ╚═╝     ╚══════╝╚═╝  ╚═╝╚══════╝ ╚═════╝ ╚═╝  ╚═══╝    ╚═╝     ╚═╝╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝ ╚═════╝ ╚══════╝╚═╝     ╚═╝╚══════╝╚═╝  ╚═══╝   ╚═╝       ╚══════╝   ╚═╝   ╚══════╝   ╚═╝   ╚══════╝╚═╝     ╚═╝╚═╝
                                                                                                                                                                                                                                                           \s
                """;

        System.out.println(banner);
    }


}
