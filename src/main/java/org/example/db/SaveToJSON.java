package org.example.db;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.example.entity.Person;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class SaveToJSON {

//    This class is responsible for saving and reading data from the JSON file.

    public SaveToJSON() {
    }

    public static void saveArrayList(ArrayList<Person> person) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(LocalDate.class, new LocalDateAdapter())
                .create();

        String json = gson.toJson(person);

        try (FileWriter file = new FileWriter("/Users/david/Desktop/perfekta-produkter-abs/src/main/java/org/example/db/person.json")) {



            file.write(json);
            file.flush();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public static ArrayList<Person> readArrayList() {
        JSONParser jsonParser = new JSONParser();
        ArrayList<Person> personList = new ArrayList<>();
        try (FileReader reader = new FileReader("/Users/david/Desktop/perfekta-produkter-abs/src/main/java/org/example/db/person.json")) {
            Object obj = jsonParser.parse(reader);

            // Since the JSON structure is an array, you can directly cast it to a JSONArray
            JSONArray personListJSON = (JSONArray) obj;

            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(LocalDate.class, new LocalDateAdapter())
                    .create();
            for (Object person : personListJSON) {
                Person person1 = gson.fromJson(person.toString(), Person.class);
                personList.add(person1);
            }
        } catch (IOException | ParseException e) {
            System.out.println(e.getMessage());
        }
        return personList;
    }


    public static void saveOrUpdatePerson(Person personToSaveOrUpdate) {
        // Read the existing data from the JSON file into a list of Person objects
        ArrayList<Person> personList = readArrayList();

        // Check if the person already exists in the list (you should define a suitable equals() method in the Person class)
        boolean personExists = false;
        Person existingPersonToUpdate = null;

        for (Person existingPerson : personList) {
            if (existingPerson.getId().equalsIgnoreCase(personToSaveOrUpdate.getId())) {
                // The person already exists, store a reference to it for possible update
                personExists = true;
                existingPersonToUpdate = existingPerson;
                break;
            }
        }

        if (personExists) {
            // Ask the user whether to update the existing person
            System.out.println("Person already exists, do you want to update it? (y/n)");
            String input = new java.util.Scanner(System.in).nextLine();
            if (input.equalsIgnoreCase("y")) {
//                    Update the existing person with new data
                updatePerson(existingPersonToUpdate, personToSaveOrUpdate);
                System.out.println("Person updated successfully.");

            } else {
                System.out.println("Person not updated.");

            }
        } else {
            // The person does not exist, add it to the list
            personList.add(personToSaveOrUpdate);
            System.out.println("Person added.");

        }

        // Write the updated list of Person objects back to the JSON file
        saveArrayList(personList);

    }


    // Update an existing person with new data
    public static void updatePerson(Person existingPerson, Person newData) {
        // Update the fields of the existing person with new data
        updateNewPerson(existingPerson, newData);

        // Now, you should update the list of Person objects in memory
        ArrayList<Person> personList = readArrayList();

        // Find and update the existing person in the list
        for (Person person : personList) {
            if (person.getId().equals(existingPerson.getId())) {
                updateNewPerson(person, newData);
                break;
            }
        }

        // Write the updated list of Person objects back to the JSON file
        saveArrayList(personList);
    }

    private static void updateNewPerson(Person existingPerson, Person newData) {
        existingPerson.setFirstName(newData.getFirstName());
        existingPerson.setLastName(newData.getLastName());
        existingPerson.setGender(newData.getGender());
        existingPerson.setStart(newData.getStart());
        existingPerson.setContractType(newData.getContractType());
        existingPerson.setSalary(newData.getSalary());
        existingPerson.setPhoneNumber(newData.getPhoneNumber());
        if (newData.getContractType().equalsIgnoreCase("interns")) {
            existingPerson.setEnd(LocalDate.parse(newData.getEnd()));
        } else{
            existingPerson.setEndNotice(newData.getEndNotice());
        }

    }


    // delete by id
    public static void deletePersonById(String id) {
        // Read the existing data from the JSON file into a list of Person objects
        ArrayList<Person> personList = readArrayList();

        // Remove the matching person
        personList.removeIf(person -> person.getId().equalsIgnoreCase(id));

        // Write the updated list of Person objects back to the JSON file
        saveArrayList(personList);
    }


}

