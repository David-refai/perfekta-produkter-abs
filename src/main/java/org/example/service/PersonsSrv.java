package org.example.service;

import lombok.Getter;
import org.example.App;
import org.example.doa.PersonDoaImpl;
import org.example.entity.Gender;
import org.example.entity.Person;
import org.example.utility.Utils;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.*;

import static org.example.validation.Validation.*;


@Getter
public class PersonsSrv {

    private static PersonsSrv personsSrvInstance;

    private final PersonDoaImpl personDoaImpl;

    private PersonsSrv() {
        this.personDoaImpl = new PersonDoaImpl();
    }

    public static PersonsSrv getInstance() {
        if (personsSrvInstance == null) {
            personsSrvInstance = new PersonsSrv();
        }
        return personsSrvInstance;
    }


    //    Method for save person in db file json (person.json)
    public void savePerson() {
        String firstName, lastName, phoneNumber, genderChoice, type, salary, description;
        boolean validInput;
        Person person = new Person();
        do {
            firstName = checkInput(Utils.printMenu("Enter First Name: "));
        } while (firstName == null);

        do {
            lastName = checkInput(Utils.printMenu("Enter Last Name: "));
        } while (lastName == null);

        do {
            phoneNumber = checkInput(Utils.printMenu("Enter Phone Number: "));
        } while (phoneNumber == null);

        do {
            genderChoice = checkInput(Utils.printMenu("Choose Gender (1️⃣-Female, 2️⃣-Male): "));
        } while (genderChoice == null);

        do {
            type = checkInput(Utils.printMenu("Enter Contract Type (1️⃣-until further notice, 2️⃣-interns): "));
        } while (type == null);
        if ("interns".equalsIgnoreCase(type)) {
            do {
                description = checkInput(Utils.printMenu("Write Description: "));
            } while (description == null);
        }
        do {
            salary = checkInput(Utils.printMenu("Enter Salary: "));
        } while (salary == null);


        // Rest of your code to capture and validate other input fields
        validInput = validateInput(firstName, lastName, phoneNumber, genderChoice, type, salary);
        if (!validInput) {
            System.out.println("Invalid input. Please try again.");
            return; // Exit the method if the input is invalid
        }


        // Validate and parse user input for gender
        checkGender(genderChoice, person);
        // Validate and parse user input for contract type and set the end date accordingly
        validContract(type, person);
        // Set the values of the Person object
        person.setId(UUID.randomUUID().toString().substring(0, 8));
        person.setFirstName(firstName);
        person.setLastName(lastName);
        person.setPhoneNumber(phoneNumber);
        person.setSalary(Double.parseDouble(salary));


        person.setCreated(LocalDate.now());


//        save person
        personDoaImpl.save(person);
        System.out.println("Person Saved Successfully " + person.getId());
    }

    //
    //    Method  for update person by id
    public void update() throws DateTimeParseException {
        if (personDoaImpl.findAll().isEmpty()) {
            System.out.println("No Person Save in db ❌");
            return;
        }
        String id = Utils.printMenu("Enter that you want to update: (note: you can't update id)");
        for (Person person : personDoaImpl.findAll()) {
            String descriptionLine;
            if (person.getId().equals(id)) {
                // Check if the contract type is "interns" to include the description
                if ("interns".equalsIgnoreCase(person.getContractType())) {
                    descriptionLine = "\n8️⃣-Description: " + person.getDescription() + "\n7️⃣-End: " + person.getEnd();
                } else {
                    descriptionLine = "\n7️⃣-EndNotice: " + person.getEndNotice();
                }
                System.out.println("\n1️⃣-First Name: " + person.getFirstName() + "\n2️⃣-Last Name: " + person.getLastName() + "\n3️⃣-Phone Number: " + person.getPhoneNumber() + "\n4️⃣-Gender: " + person.getGender() + "\n5️⃣-Type: " + person.getContractType() + "\n6️⃣-Start: " + person.getStart() + descriptionLine + "\n9️⃣-Salary: " + person.getSalary() + "\n🅾️- Back to main menu");
                String choice = Utils.printMenu("Choose an option: ");

                switch (choice) {
                    case "0" -> App.system();

                    case "1" -> {
                        String firstName = Utils.printMenu("Enter First Name: ");
                        person.setFirstName(firstName);
                    }
                    case "2" -> {
                        String lastName = Utils.printMenu("Enter Last Name: ");
                        person.setLastName(lastName);
                    }
                    case "3" -> {
                        String phoneNumber = Utils.printMenu("Enter Phone Number: ");
                        person.setPhoneNumber(phoneNumber);
                    }
                    case "4" -> {
                        String genderChoice = Utils.printMenu("Choose Gender (1️⃣-Female, 2️⃣-Male): ");
                        checkGender(genderChoice, person);
                    }

                    case "5" -> {
                        String type = Utils.printMenu("Enter Contract Type (1️⃣-until further notice, 2️⃣-interns): ");
                        validContract(type, person);
                    }


                    case "6" -> {
                        String start = Utils.printMenu("When will start (YYYY-MM-DD): ");
                        try {
                            person.setStart(LocalDate.parse(start));
                        } catch (DateTimeParseException e) {
                            System.out.println("Invalid date format for start date. Please use YYYY-MM-DD.");
                            return; // Exit the method if the date format is invalid
                        }
                    }
                    case "7" -> {
                        if ("interns".equalsIgnoreCase(person.getContractType())) {
                            String end = Utils.printMenu("When finish the contract (YYYY-MM-DD): ");
                            try {
                                person.setEnd(checkDate(LocalDate.parse(end)));
                            } catch (DateTimeParseException e) {
                                System.out.println("Invalid date format for end date. Please use YYYY-MM-DD.");
                                return; // Exit the method if the date format is invalid
                            }
                        } else {
                            String endNotice = Utils.printMenu("Enter End Notice: ");
                            person.setEndNotice(endNotice);
                        }
                    }
                    case "8" -> {
                        String description = Utils.printMenu("Enter Description: ");
                        person.setDescription(description);

                    }
                    case "9" -> {
                        String salary = Utils.printMenu("Enter Salary: ");
                        person.setSalary(Double.parseDouble(salary));
                    }


                    default -> System.out.println("Invalid input");
                }

                String question = Utils.printMenu("Do you want to save changes? (Y/N): ");
                if (question.equalsIgnoreCase("Y")) {
                    personDoaImpl.save(person);
                    System.out.println("Updated Successfully");
                } else if (question.equalsIgnoreCase("N")) {
                    System.out.println("Not Updated");
                } else {
                    System.out.println("Invalid input");
                }


            }

        }
        if (personDoaImpl.findById(id) == null || !personDoaImpl.findById(id).getId().equals(id)) {
            System.out.println("Not found " + id);
        }


    }

    //!    Method for delete person by id
    public void delete() {
        String id = Utils.printMenu("Enter Id that you want to delete: XXXXXXXX");
        Optional<Person> person = Optional.ofNullable(personDoaImpl.findById(id));
        if (person.isPresent()) {
            personDoaImpl.delete(id);
            System.out.println("Deleted Successfully");
        } else {
            System.out.println("Not found");
        }
    }

    //!    Method for show all person
    public void findAll() {

        var list = personDoaImpl.findAll();
        if (list.isEmpty()) {
            System.out.println("-".repeat(50) + "\nNo Person Found ❌\n" + "-".repeat(50));
        } else {
            System.out.println("-".repeat(50) + "\nPersons List\nYou have: " + list.size() + " items\n" + "-".repeat(50));
        }

//        Sort the list by end date
        list.sort(Comparator.comparing(Person::getEnd));

        for (Person person : list) {
//!            Check if the contract type is "interns" to include the description
            String descriptionLine;
            if ("interns".equalsIgnoreCase(person.getContractType())) {
                descriptionLine = "\nDescription: " + person.getDescription() + "\nEnd: " + person.getEnd();
            } else {
                descriptionLine = "\nEndNotice: " + person.getEndNotice();
            }
            System.out.println("{\nId: " + person.getId() + "\nFirst Name: " + person.getFirstName() + "\nLast Name: " + person.getLastName() + "\nPhone Number: " + person.getPhoneNumber() + "\nGender: " + person.getGender() + "\nType: " + person.getContractType() + "\nStart: " + person.getStart() + descriptionLine + "\nSalary: " + person.getSalary() + "\n}"


            );


        }
        System.out.println("\nYou have: " + list.size() + " items in list ☝️");

    }

    public void findById() {
        var id = Utils.printMenu("Enter Id:  XXXXXXXX");
        Optional<Person> person = Optional.ofNullable(personDoaImpl.findById(id));
        if (person.isPresent()) {
            new Utils().printList("Person Found ", person);
        } else {
            System.out.println("Not found");
        }

    }

    public void findByGender() {
        var list = personDoaImpl.findByGender();
        String genderChoice = Utils.printMenu("Enter gender:\n1️⃣-Male\n2️⃣-Female");

        if (genderChoice.equals("1")) {
            list.stream().filter(person -> person.getGender() == Gender.MALE).sorted(Comparator.comparing(Person::getStart)).forEach(System.out::println);
        } else if (genderChoice.equals("2")) {
            list.stream().filter(person -> person.getGender() == Gender.FEMALE).sorted(Comparator.comparing(Person::getEnd)).forEach(System.out::println);
        } else {
            System.out.println("Invalid gender choice.");
        }
    }


    public void search() {
        var loop = true;
        while (loop) {
            Utils.menu("""
                    You have more alternative for searching
                    1️⃣-By Id
                    2️⃣-By Gender
                    3️⃣-By Contract Type
                    4️⃣← Back to main menu
                    """);

            var input = Utils.printMenu("Choose an option: ");
            switch (Integer.parseInt(input)) {
                case 1 -> findById();
                case 2 -> findByGender();
                case 3 -> findByContractType();
                case 4 -> App.system();

                default -> {
                    System.out.println("Invalid input");
                    loop = false;
                }
            }

        }

    }


    private void findByContractType() {
        var list = personDoaImpl.findAll();
        String type = Utils.printMenu("Enter Contract Type:\n1️⃣-until further notice\n2️⃣-interns");

        if (type.equals("1")) {
            list.stream().filter(person -> person.getContractType().equalsIgnoreCase("until further notice")).sorted(Comparator.comparing(Person::getEnd)).forEach(System.out::println);
        }
        if (type.equals("2")) {

            list.stream().filter(person -> person.getContractType().equalsIgnoreCase("interns")).sorted(Comparator.comparing(Person::getEnd)).forEach(System.out::println);
        } else {
            System.out.println("Invalid contract type.");
        }
    }


    public Map<Gender, Double> calculateAverageSalaryByGender(List<Person> Persons) {
        Map<Gender, Double> averageSalaryByGender = new HashMap<>();

        for (Gender gender : Gender.values()) {
            List<Person> genderFilteredPersons = Persons.stream().filter(Person -> Person.getGender() == gender).toList();
            System.out.println("-".repeat(50));

            averageSalary(averageSalaryByGender, gender, genderFilteredPersons);

        }

        return averageSalaryByGender;
    }

    private void averageSalary(Map<Gender, Double> averageSalaryByGender, Gender gender, List<Person> genderFilteredPersons) {
        double totalSalary = genderFilteredPersons.stream().mapToDouble(Person::getSalary).sum();
        System.out.println("Total: " + totalSalary);

        double averageSalary = genderFilteredPersons.isEmpty() ? 0.0 : totalSalary / genderFilteredPersons.size();
        averageSalaryByGender.put(gender, averageSalary);

        System.out.println("Average: " + averageSalary);
        System.out.println("-".repeat(50));
    }
}




