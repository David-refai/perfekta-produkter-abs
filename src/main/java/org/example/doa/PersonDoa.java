package org.example.doa;

import org.example.entity.Person;

import java.util.List;

public interface PersonDoa {


    void save(Person person);

    void delete(String id);

    List<Person> findAll();

    Person findById(String id);

    List<Person> findByGender();


}
