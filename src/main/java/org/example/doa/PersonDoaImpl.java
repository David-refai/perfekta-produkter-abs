package org.example.doa;

import lombok.Getter;
import org.example.db.SaveToJSON;
import org.example.entity.Person;

import java.util.ArrayList;
import java.util.List;

@Getter
public class PersonDoaImpl implements PersonDoa {


    //!   💉 Injecting the SaveArrayListToJSON class into the PersonDoaImpl class, that is responsible for reading and writing to the JSON file. ✈️
    ArrayList<Person> personList = new ArrayList<>();
    SaveToJSON saveToJSON;

    public PersonDoaImpl() {
    }


    @Override
    public void save(Person person) {
        personList.add(person);
        SaveToJSON.saveOrUpdatePerson(person);

    }

    @Override
    public void delete(String id) {
        var person = findById(id);
        SaveToJSON.deletePersonById(person.getId());
    }

    @Override
    public List<Person> findAll() {
        return SaveToJSON.readArrayList();
    }

    @Override
    public Person findById(String id) {
        return SaveToJSON.readArrayList().stream().filter(person -> person.getId().equals(id)).findFirst().orElse(null);
    }


    @Override
    public List<Person> findByGender() {
        return SaveToJSON.readArrayList();
    }


}
