package org.example.utility;

import java.util.Scanner;

public class Utils {

//    This class is responsible for printing the menu and getting the user's input.

    public static void menu(String... args) {
        System.out.println("-".repeat(30));
        for (String arg : args) {

            System.out.printf(" %s\n", arg);
        }
    }



    public static String printMenu(String s) {
        System.out.println("-".repeat(30));
        Scanner scanner = new Scanner(System.in);
        System.out.println(s);
        return scanner.nextLine();

    }

    public <T> void printList(String s, T o) {
        System.out.println("-".repeat(30));
        System.out.println(s);
        System.out.println("-".repeat(30));
        System.out.println(o);
        System.out.println("-".repeat(30));
    }

}
