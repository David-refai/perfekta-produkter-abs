package org.example.validation;

import org.example.App;
import org.example.doa.PersonDoaImpl;
import org.example.entity.Gender;
import org.example.entity.Person;
import org.example.utility.Utils;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public class Validation {

    public static void validContract(String input, Person person) {
        LocalDate startDate;
        LocalDate endDate;
        if(input.equals("1")) {
            person.setContractType("indefinite");
            String start = Utils.printMenu("When will start (YYYY-MM-DD): ");
            String  end = Utils.printMenu("When finish the contract (YYYY-MM-DD):\n** note: if you don't enter a date, it will be 👉 Indefinitely");
            try {
                startDate = LocalDate.parse(start.isEmpty() ? LocalDate.now().toString() : start);
                if (!end.isEmpty()){
                    endDate = checkDate(LocalDate.parse(end));
                    person.setEnd(endDate);
                }else {
                 person.setEndNotice("Indefinite");
                }
                person.setStart(startDate);
            } catch (DateTimeParseException e) {
                System.out.println("Invalid date format. Please use YYYY-MM-DD.");
            }
        } else if (input.equals("2")) {
            person.setContractType("Interns");
            String start = Utils.printMenu("When will start (YYYY-MM-DD): ");
            String  end = Utils.printMenu("When finish the contract (YYYY-MM-DD):\n** note: if you don't enter a date, it will be 1 year later");
            try {
                startDate = LocalDate.parse(start.isEmpty() ? LocalDate.now().toString() : start);
            endDate = LocalDate.parse(end.isEmpty() ? startDate.plusYears(1).toString() : end);

                // Add a condition to check if the end date is after the start date
            if (endDate.isBefore(startDate)) {
                System.out.println("End date must be after the start date.");
            }
                person.setStart(startDate);
                person.setEnd(endDate);
            } catch (DateTimeParseException e) {
                System.out.println("Invalid date format. Please use YYYY-MM-DD.");
            }
        } else {
            System.out.println("Invalid contract type. Defaulting to until further notice.");
            person.setContractType("until further notice");
        }
    }

    public static void checkGender(String genderChoice, Person person) {
        Gender gender;
        try {

            int genderCode = Integer.parseInt(genderChoice);
            if (genderCode == 1) {
                gender = Gender.FEMALE;
            } else if (genderCode == 2) {
                gender = Gender.MALE;
            } else {
                System.out.println("Invalid gender choice. Defaulting to unspecified.");
                gender = Gender.UNSPECIFIED;
            }
            person.setGender(gender);
        } catch (NumberFormatException e) {
            System.out.println("Invalid gender choice. Defaulting to unspecified.");
            gender = Gender.UNSPECIFIED;
            person.setGender(gender);
        }
    }

    public static String checkInput(String input) {

        while (input.isEmpty()) {
            System.out.println("Invalid input. Please try again.");

            String choice = Utils.printMenu("Do you want to continue? (Y/N): ");
            if (choice.equalsIgnoreCase("Y")) {
                return null; // Signal to continue the input loop
            } else if (choice.equalsIgnoreCase("N")) {
                App.getInstance().system(); // Return to the main menu
                return null; // Signal to continue the input loop
            } else {
                System.out.println("Invalid input. Please try again.");
            }
        }

        return input; // Return the valid input
    }


    public static boolean validateInput(String firstName, String lastName, String phoneNumber, String genderChoice, String type, String salary) {

        boolean isGenderChoiceValid = genderChoice.equals("1") || genderChoice.equals("2");
        boolean isTypeValid = type.equals("1") || type.equals("2");
        boolean isSalaryNumeric = salary.matches("\\d+") || salary.matches("\\d+\\.\\d+");
        boolean isPhoneNumberNumeric = phoneNumber.matches("\\d+") || phoneNumber.isEmpty() || phoneNumber.isBlank();
        boolean isSalaryPositive = Double.parseDouble(salary) > 0;
        return !firstName.isEmpty() && !lastName.isEmpty() && !phoneNumber.isEmpty() &&
                isGenderChoiceValid && isTypeValid && isSalaryNumeric && isSalaryPositive && isPhoneNumberNumeric;
    }

    public static LocalDate checkDate(LocalDate dateToCheck) {
        PersonDoaImpl personDoaImpl = new PersonDoaImpl();

        // Compare dateToCheck with the "start date" and "end date" of each person
        boolean dateWithinRange = personDoaImpl.findAll().stream()
                .filter(person ->
                        !dateToCheck.isBefore(person.getStart()) &&
                                !dateToCheck.isAfter(LocalDate.parse(person.getEnd()))).isParallel();

        if (dateWithinRange) {
            return dateToCheck;
        } else {
            System.err.println("Date is not within the start date and end date range of any person.");
            return null;
        }
    }
    }


