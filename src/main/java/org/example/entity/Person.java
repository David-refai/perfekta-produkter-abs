package org.example.entity;

import lombok.*;
import java.time.LocalDate;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ToString
@EqualsAndHashCode
public class Person {
    private static Person personInstance;
    private String id;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private Gender gender;
    private LocalDate created;
    private String contractType;
    private double salary;
    private LocalDate start;
    private LocalDate end;
    private String description;
    private String endNotice;


    public String getEnd() {
        if (end != null) {
            return end.toString(); // Return the date as a string
        } else {
            return endNotice; // Return "until further notice"
        }

    }
}
